@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">'Modulo De Productos'</div>
                <div class="col tex-right">
                <a href="{{route('crear.producto')}}"class="btn-sm btn-primary">nuevo producto</a>
</div>
                <div class="card-body">
                <table class="table">
  <thead>
    <tr>
      <th scope="col"># ID</th>
      <th scope="col">NOMBRE</th>
      <th scope="col">TIPO</th>
      <th scope="col">ESTADO</th>
      <th scope="col">PRECIO</th>
    </tr>
  </thead>
  <tbody>
  @foreach ($cliente as $item)
    <tr>
      <th scope="row">{{$item-id}}</th>
      <td>{{$item-nombre}}</td>
      <td>{{$item-tipo}}</td>
      <td>{{$item-estado}}</td>
      <td>{{$item-precio}}</td>
    </tr>
    @endforeach
  </tbody>
</table>


                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
