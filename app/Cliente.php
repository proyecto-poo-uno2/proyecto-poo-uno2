<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    Protected $fillable =[
        'nombre',
        'apellido',
        'cedula',
        'direccion',
        'telefono',
        'fecha_nacimiento',
        'email'
    ];
}
